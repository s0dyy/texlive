# Copyright 2013 Thomas Witt
# Distributed under the terms of the GNU General Public License v2

require texlive-common

SUMMARY="Create glossaries and lists of acronyms"
HOMEPAGE="http://www.ctan.org/pkg/glossaries"
DOWNLOADS="http://dev.exherbo.org/distfiles/texlive/2014/${PNV}.zip"

LICENCES="LPPL-1.3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="doc examples"

DEPENDENCIES="
    build:
        app-arch/unzip
        examples? (
            app-text/dvipsk
            dev-tex/xcolor
        )
    build+run:
        app-text/texlive-core
        dev-texlive/texlive-latex
    test:
        dev-texlive/texlive-fontsextra
        dev-texlive/texlive-latexextra
        dev-texlive/texlive-latexrecommended
"

BUGS_TO="pyromaniac@thwitt.de"

WORK="${WORKBASE}/${PN}"

src_compile() {
    edo latex "${PN}.ins"

    if optionq examples; then
        edo pdflatex -jobname glossaries-code glossaries.dtx
        edo makeindex -s gind.ist glossaries-code.idx
        edo makeindex -s gglo.ist -o glossaries-code.gls glossaries-code.glo
        edo "${WORK}/makeglossaries" glossaries-code
        edo pdflatex -jobname glossaries-code glossaries.dtx
        edo pdflatex -jobname glossaries-code glossaries.dtx
    fi
}

src_test() {
    edo latex minimalgls
    edo "${WORK}/makeglossaries" minimalgls
    edo latex minimalgls
}

TEX_PATH="/usr/share/texmf-site"
src_install() {
    insinto "${TEX_PATH}/tex/latex/glossaries/base"
    doins "glossaries.sty" "glossaries-babel.sty" "glossaries-polyglossia.sty" "glossaries-compatible-207.sty" "mfirstuc.sty"

    insinto "${TEX_PATH}/tex/latex/glossaries/expl"
    doins "glossaries-accsupp.sty"

    insinto "${TEX_PATH}/tex/latex/glossaries/styles"
    doins glossary-*.sty

    insinto "${TEX_PATH}/tex/latex/glossaries/dict"
    doins *.dict

    dobin makeglossaries

    option examples && dodoc -r samples
    option doc  && dodoc *.pdf

    emagicdocs
}

pkg_postinst() {
    etexmf-update
}

pkg_postrm() {
    etexmf-update
}

