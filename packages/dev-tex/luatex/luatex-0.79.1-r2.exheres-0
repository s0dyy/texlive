# Copyright 2011 Ingmar Vanhassel
# Copyright 2013 Ingmar Vanhassel
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'luatex-0.65.0.ebuild' which is
#   Copyright 1999-2011 Gentoo Foundation

require texlive-common

SUMMARY="An extended version of pdfTeX using Lua as an embedded scripting language"
DESCRIPTION="
The LuaTeX projects main objective is to provide an open and configurable variant of TeX while at
the same time offering downward compatibility.
"

# NOTE: Update ${FILE_ID} in sync with the version. If you don't it'll silently fetch the wrong
#       version.
MY_PNV="${PN}-beta-${PV}"
FILE_ID=15763

HOMEPAGE="http://www.luatex.org/"
DOWNLOADS="http://foundry.supelec.fr/frs/download.php/file/${FILE_ID}/${MY_PNV}.tar.bz2"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        app-text/poppler[<0.58]
        dev-libs/kpathsea
        dev-libs/zziplib
        media-libs/libpng:=
        sys-libs/zlib
        x11-libs/cairo
        x11-libs/pixman:1
"

BUGS_TO="thomas@thwitt.de"

WORK="${WORKBASE}/${MY_PNV}/source/texk/web2c"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-luatex

    --disable-cxx-runtime-hack
    --disable-all-pkgs
    --disable-multiplatform
    --disable-ptex
    --disable-tex
    --disable-mf
    --disable-ipc
    --disable-shared
    --without-mf-x-toolkit
    --without-x

    --with-system-{gd,kpathsea,libpng,poppler,cairo,pixman,t1lib,teckit,xpdf,zlib,zziplib}
    --with-kpathsea-includes=/usr/$(exhost --target)/include
)

DEFAULT_SRC_INSTALL_EXCLUDE=( packaging.c )

src_configure() {
    default

    edo pushd ../../libs/lua52
    econf
    edo popd

    #edo cd ../../libs/obsdcompat
    #econf

    edo pushd ../../
    edo texk/web2c/luatexdir/getluatexsvnversion.sh
    edo popd
}

src_compile() {
    emake AR=${AR} luatex
}

src_install() {
    # some subdirectories lack an 'install' target, so 'make install' aborts
    emake install-exec-am DESTDIR="${IMAGE}" bin_PROGRAMS="luatex" # SUBDIRS="" nodist_man_MANS=""

    # these symlinks are handled by texlive-core TODO proper auto-toolisation
    edo rm "${IMAGE}"/usr/$(exhost --target)/bin/{dvitomp,mfplain,texluajit{,c}}

    emagicdocs

    emake -C man luatex.1
    doman man/luatex.1
}

pkg_postinst() {
    efmtutil-sys
}

